<?php

namespace Drupal\unitsapi\Plugin;

use Drupal\unitsapi\Measurement;

/**
 * Defines an interface for derived Units API property plugins.
 */
interface UnitsApiDerivedPropertyInterface extends UnitsApiPropertyInterface {

  /**
   * Returns the measurement key for this obj based on passed in measurement.
   *
   * @param \Drupal\unitsapi\Measurement $measurement
   *   The measurement to base the key on.
   *
   * @return string
   *   The measurement key.
   */
  public function getMeasurementKey(Measurement $measurement);

  /**
   * Hydrates passed in Measurement with derived quantity.
   *
   * Requires that all all required measurements have been added and that the
   * measurement property type matches this property type.
   *
   * @param \Drupal\unitsapi\Measurement $measurement
   *   The derived property measurement to hydrate with info from this object.
   *
   * @throws \Drupal\unitsapi\Exception\ConversionException
   *   Thrown when derived measurement can't be hydrated for validation reasons.
   */
  public function hydrateDerivedMeasurement(Measurement $measurement);

  /**
   * Returns a calculated measurement of the final required measurement.
   *
   * Requires that the Measurement param is a fully hydrated derived measurement
   * and that only a single required measurement remains.
   *
   * @param \Drupal\unitsapi\Measurement $measurement
   *   The hydrated derived property measurement.
   * @param string|null $to_unit
   *   The unit to convert the returned Measurement to. If left as NULL, the
   *   default unit will be returned. Must be a valid unit plugin id matching
   *   the property of the remaining required measurement.
   *
   * @throws \Drupal\unitsapi\Exception\ConversionException
   *   Thrown when the base can't be calculated for validation reasons.
   *
   * @return \Drupal\unitsapi\Measurement
   *   A Measurement object matching the remaining required measurement
   *   containing the calculated quantity.
   */
  public function getCalculatedBase(Measurement $measurement, $to_unit);

  /**
   * Adds a passed in base measurements obj to this property.
   *
   * @param \Drupal\unitsapi\Measurement $measurement
   *   The base measurement obj to add to this property.
   */
  public function addBaseMeasurement(Measurement $measurement);

}
