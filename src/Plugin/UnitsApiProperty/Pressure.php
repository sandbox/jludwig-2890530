<?php

namespace Drupal\unitsapi\Plugin\UnitsApiProperty;

use Drupal\unitsapi\Plugin\UnitsApiDerivedPropertyBase;

/**
 * Provides the Pressure UnitsAPI property.
 *
 * @UnitsApiProperty(
 *   id = "pressure",
 *   label = @Translation("Pressure"),
 *   defaultUnit = "pascal",
 *   baseMeasurements = {
 *     "force" = {
 *       "unit" = "newton"
 *     },
 *     "area" = {
 *       "unit" = "square_meter"
 *     }
 *   },
 *   maxCalcDepth = 2
 * )
 */
class Pressure extends UnitsApiDerivedPropertyBase {

  /**
   * {@inheritdoc}
   */
  protected function calcDerivedQuantity() {
    $force = $this->getBaseQuantity('force');
    $area = $this->getBaseQuantity('area');
    if (empty($area)) {
      return NULL;
    }
    return $force / $area;
  }

  /**
   * {@inheritdoc}
   */
  protected function calcBaseMeasurement($quantity) {
    if (isset($this->requiredMeasurements['force'])) {
      $area = $this->getBaseQuantity('area');
      return $quantity * $area;
    }

    if (isset($this->requiredMeasurements['area'])) {
      $force = $this->getBaseQuantity('force');
      if (empty($quantity)) {
        return NULL;
      }
      return $force / $quantity;
    }
  }

}
