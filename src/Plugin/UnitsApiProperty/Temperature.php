<?php

namespace Drupal\unitsapi\Plugin\UnitsApiProperty;

use Drupal\unitsapi\Exception\ConversionException;
use Drupal\unitsapi\Measurement;
use Drupal\unitsapi\Plugin\UnitsApiPropertyBase;

/**
 * Provides the Temperature UnitsAPI property.
 *
 * @UnitsApiProperty(
 *   id = "temperature",
 *   label = @Translation("Temperature"),
 *   defaultUnit = "kelvin",
 *   customFactors = true
 * )
 */
class Temperature extends UnitsApiPropertyBase {

  /**
   * A map of methods used for unit conversion.
   *
   * @var array
   */
  protected $unitsMethodMap = [
    'celsius' => 'convertFromCelsius',
    'fahrenheit' => 'convertFromFahrenheit',
    'kelvin' => 'convertFromKelvin',
  ];

  /**
   * {@inheritdoc}
   */
  public function convertUnit(Measurement $from_measurement, $to_unit) {
    if ($from_measurement->getUnitId() === $to_unit) {
      return $from_measurement->getQuantity();
    }

    /** @var Drupal\unitsapi\Measurement $to_measurement */
    $to_measurement = $this->measurementManager->createMeasurement($to_unit);
    $this->validateUnitConversion($from_measurement, $to_measurement);

    $method = $this->unitsMethodMap[$from_measurement->getUnitId()];

    $to_measurement->setQuantity($this->$method(
      $from_measurement->getQuantity(),
      $to_measurement->getUnitId()
    ));

    return $to_measurement;
  }

  /**
   * {@inheritdoc}
   */
  protected function validateUnitConversion(Measurement $from_measurement, Measurement $to_measurement) {
    parent::validateUnitConversion($from_measurement, $to_measurement);

    if (!isset($this->unitsMethodMap[$from_measurement->getUnitId()])) {
      throw new ConversionException(sprintf('UnitsAPI does not know how to convert a temperature of the unit "%s"', $from_measurement->getUnitId()));
    }
    if (!isset($this->unitsMethodMap[$to_measurement->getUnitId()])) {
      throw new ConversionException(sprintf('UnitsAPI does not know how to convert to temperature of the unit "%s"', $to_measurement->getUnitId()));
    }
  }

  /**
   * Conversion method for converting from degrees Celsius.
   *
   * @param float|int $from_quantity
   *   The number of degrees Celsius before converting.
   * @param string $to_unit
   *   The plugin id of the unit to convert to.
   *
   * @return float|int
   *   Returns the quantity of degrees in the requested unit.
   */
  protected function convertFromCelsius($from_quantity, $to_unit) {
    if ($to_unit === 'fahrenheit') {
      return ($from_quantity * (9 / 5)) + 32;
    }
    if ($to_unit === 'kelvin') {
      return $from_quantity + 273.15;
    }
  }

  /**
   * Conversion method for converting from degrees Fahrenheit.
   *
   * @param float|int $from_quantity
   *   The number of degrees Fahrenheit before converting.
   * @param string $to_unit
   *   The plugin id of the unit to convert to.
   *
   * @return float|int
   *   Returns the quantity of degrees in the requested unit.
   */
  protected function convertFromFahrenheit($from_quantity, $to_unit) {
    if ($to_unit === 'celsius') {
      return ($from_quantity - 32) * (5 / 9);
    }
    if ($to_unit === 'kelvin') {
      return ($from_quantity - 32) * (5 / 9) + 273.15;
    }
  }

  /**
   * Conversion method for converting from degrees Kelvin.
   *
   * @param float|int $from_quantity
   *   The number of degrees Kelvin before converting.
   * @param string $to_unit
   *   The plugin id of the unit to convert to.
   *
   * @return float|int
   *   Returns the quantity of degrees in the requested unit.
   */
  protected function convertFromKelvin($from_quantity, $to_unit) {
    if ($to_unit === 'celsius') {
      return $from_quantity - 273.15;
    }
    if ($to_unit === 'fahrenheit') {
      return ($from_quantity - 273.15) * (9 / 5) + 32;
    }
  }

}
