<?php

namespace Drupal\unitsapi\Plugin\UnitsApiProperty;

use Drupal\unitsapi\Plugin\UnitsApiDerivedPropertyBase;

/**
 * Provides the Density UnitsAPI property.
 *
 * @UnitsApiProperty(
 *   id = "density",
 *   label = @Translation("Density"),
 *   defaultUnit = "kilogram_per_cubic_meter",
 *   baseMeasurements = {
 *     "mass" = {
 *       "unit" = "kilogram"
 *     },
 *     "volume" = {
 *       "unit" = "cubic_meter"
 *     }
 *   }
 * )
 */
class Density extends UnitsApiDerivedPropertyBase {

  /**
   * {@inheritdoc}
   */
  protected function calcDerivedQuantity() {
    $mass = $this->getBaseQuantity('mass');
    $volume = $this->getBaseQuantity('volume');
    if (empty($volume)) {
      return NULL;
    }
    return $mass / $volume;
  }

  /**
   * {@inheritdoc}
   */
  protected function calcBaseMeasurement($quantity) {
    if (isset($this->requiredMeasurements['mass'])) {
      $volume = $this->getBaseQuantity('volume');
      return $quantity * $volume;
    }

    if (isset($this->requiredMeasurements['volume'])) {
      $mass = $this->getBaseQuantity('mass');
      if (empty($quantity)) {
        return NULL;
      }
      return $mass / $quantity;
    }
  }

}
