<?php

namespace Drupal\unitsapi\Plugin\UnitsApiProperty;

use Drupal\unitsapi\Plugin\UnitsApiPropertyBase;

/**
 * Provides the Length UnitsAPI property.
 *
 * @UnitsApiProperty(
 *   id = "length",
 *   label = @Translation("Length"),
 *   defaultUnit = "meter"
 * )
 */
class Length extends UnitsApiPropertyBase {
}
