<?php

namespace Drupal\unitsapi;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\Discovery\ContainerDerivativeDiscoveryDecorator;
use Drupal\Core\Plugin\Discovery\YamlDiscovery;
use Drupal\unitsapi\Plugin\UnitsApiPropertyManager;

/**
 * Provides the default unitsapi_unit manager.
 */
class UnitsApiUnitManager extends DefaultPluginManager {

  /**
   * Provides default values for all unitsapi_unit plugins.
   *
   * @var array
   */
  protected $defaults = [
    'id' => '',
    'unit_property' => '',
    'singular' => '',
    'plural' => '',
    'symbol' => '',
    'factor' => '',
    'source' => '',
  ];

  /**
   * The Unit Property plugin definitions.
   *
   * @var array
   */
  protected $propertyDefinitions = [];

  /**
   * Constructs a new UnitsApiUnitManager object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\unitsapi\UnitsApiPropertyManager $property_manager
   *   The UnitsAPI property manager.
   */
  public function __construct(ModuleHandlerInterface $module_handler, CacheBackendInterface $cache_backend, UnitsApiPropertyManager $property_manager) {
    $this->moduleHandler = $module_handler;
    $this->setCacheBackend($cache_backend, 'unitsapi_unit', ['unitsapi_unit']);
    $this->propertyDefinitions = $property_manager->getDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  protected function getDiscovery() {
    if (!isset($this->discovery)) {
      $this->discovery = new YamlDiscovery('unitsapi.unit', $this->moduleHandler->getModuleDirectories());
      $this->discovery->addTranslatableProperty('singular', 'singular_context');
      $this->discovery->addTranslatableProperty('plural', 'plural_context');
      $this->discovery->addTranslatableProperty('symbol', 'symbol_context');
      $this->discovery = new ContainerDerivativeDiscoveryDecorator($this->discovery);
    }
    return $this->discovery;
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id) {
    parent::processDefinition($definition, $plugin_id);

    $required_strings = ['id', 'unit_property', 'singular', 'plural'];
    foreach ($required_strings as $required_def) {
      if (empty($definition[$required_def])) {
        throw new PluginException(sprintf('Plugin (%s) definition "%s" is required.', $plugin_id, $required_def));
      }
    }

    $unit_property = $this->getUnitPropertyDefinition($plugin_id, $definition['unit_property']);

    if (isset($unit_property['customFactors']) && $unit_property['customFactors']) {
      return;
    }

    if (empty($definition['factor'])) {
      throw new PluginException(sprintf('Plugin (%s) definition "factor" is required.', $plugin_id));
    }

    $this->processFactor($definition['factor']);
  }

  /**
   * Returns a unit property plugin definition.
   *
   * @param string $plugin_id
   *   The plugin id of the plugin being processed.
   * @param string $unit_property_id
   *   The plugin id of the unit property.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   Thrown if unit property plugin doesn't exist.
   *
   * @return array
   *   The unit property plugin definition.
   */
  protected function getUnitPropertyDefinition($plugin_id, $unit_property_id) {
    if (empty($this->propertyDefinitions[$unit_property_id])) {
      throw new PluginException(sprintf('No valid unit property found for plugin (%s) "unit_property" definition: "%s"', $plugin_id, $unit_property_id));
    }
    return $this->propertyDefinitions[$unit_property_id];
  }

  /**
   * Processes factor to normalize the format.
   *
   * @param string $factor
   *   The unit factor.
   */
  protected function processFactor(&$factor) {
    $factor = str_replace(' ', '', $factor);
    $factor = str_replace(['—', '−', '–'], '-', $factor);

    return $factor;
  }

  /**
   * Returns the unit plugin definitions sorted by unit property.
   *
   * @param array|null $definitions
   *   The unit plugin definitions to sort. If left blank, the default
   *   definitions will be used.
   *
   * @return array
   *   The sorted unit plugin definitions.
   */
  public function getSortedUnits(array $definitions = NULL) {
    $definitions = isset($definitions) ? $definitions : $this->getDefinitions();
    uasort($definitions, function ($unit_a, $unit_b) {
      return strnatcasecmp($unit_a['unit_property'], $unit_b['unit_property']);
    });
    return $definitions;
  }

  /**
   * Returns the unit plugin definitions, grouped by unit property.
   *
   * @param array|null $definitions
   *   The unit plugin definitions to group. If left blank, the default
   *   definitions will be used.
   *
   * @return array
   *   The grouped unit plugin definitions.
   */
  public function getGroupedUnits(array $definitions = NULL) {
    $definitions = $this->getSortedUnits(isset($definitions) ? $definitions : $this->getDefinitions());
    $grouped_definitions = [];
    foreach ($definitions as $id => $definition) {
      $grouped_definitions[(string) $definition['unit_property']][$id] = $definition;
    }
    return $grouped_definitions;
  }

}
