<?php

namespace Drupal\unitsapi\Plugin\UnitsApiProperty;

use Drupal\unitsapi\Plugin\UnitsApiDerivedPropertyBase;

/**
 * Provides the Area UnitsAPI property.
 *
 * @UnitsApiProperty(
 *   id = "area",
 *   label = @Translation("Area"),
 *   defaultUnit = "square_meter",
 *   baseMeasurements = {
 *     "length" = {
 *       "unit" = "meter",
 *       "count" = 2
 *     }
 *   }
 * )
 */
class Area extends UnitsApiDerivedPropertyBase {

  /**
   * {@inheritdoc}
   */
  protected function calcDerivedQuantity() {
    $length_1 = $this->getBaseQuantity('length');
    $length_2 = $this->getBaseQuantity('length', 1);
    return $length_1 * $length_2;
  }

  /**
   * {@inheritdoc}
   */
  protected function calcBaseMeasurement($quantity) {
    reset($this->baseMeasurements['length']);
    $length = current($this->baseMeasurements['length'])->getQuantity();
    if (empty($length)) {
      return NULL;
    }
    return $quantity / $length;
  }

}
