<?php

namespace Drupal\unitsapi;

use Drupal\unitsapi\Plugin\UnitsApiPropertyManager;

/**
 * The UnitsAPI Converter service.
 *
 * @package Drupal\unitsapi
 */
class Converter implements ConverterInterface {

  /**
   * The UnitsAPI measurement manager.
   *
   * @var \Drupal\unitsapi\MeasurementManager
   */
  protected $measurementManager;

  /**
   * Drupal\unitsapi\Plugin\UnitsApiPropertyManager definition.
   *
   * @var \Drupal\unitsapi\Plugin\UnitsApiPropertyManager
   */
  protected $propertyManager;

  /**
   * Constructs a new Converter object.
   *
   * @param \Drupal\unitsapi\MeasurementManager $measurement_manager
   *   The UnitsAPI measurement manager service.
   * @param \Drupal\unitsapi\UnitsApiPropertyManager $property_manager
   *   The UnitsAPI property manager.
   */
  public function __construct(MeasurementManager $measurement_manager, UnitsApiPropertyManager $property_manager) {
    $this->measurementManager = $measurement_manager;
    $this->propertyManager = $property_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function convertUnit($quantity, $from_unit, $to_unit, $log_input = FALSE) {
    /** @var Drupal\unitsapi\Measurement $from_measurement */
    $from_measurement = $this->measurementManager->createMeasurement($from_unit, $quantity);

    /** @var Drupal\unitsapi\Measurement $from_measurement */
    $to_measurement = $this->convertMeasurementUnit($from_measurement, $to_unit);

    if ($log_input) {
      $to_measurement->logInput(['_from_measurement' => $from_measurement]);
    }

    return $to_measurement;
  }

  /**
   * {@inheritdoc}
   */
  public function convertMeasurementUnit(MeasurementInterface $from_measurement, $to_unit) {
    /** @var Drupal\unitsapi\Plugin\UnitsApiPropertyInterface $property */
    $property = $from_measurement->getUnitProperty();
    return $property->convertUnit($from_measurement, $to_unit);
  }

  /**
   * {@inheritdoc}
   */
  public function createDerivedMeasurement(array $base_values, $to_unit, $log_input = FALSE) {
    /** @var Drupal\unitsapi\Measurement[] $base_measurements */
    $base_measurements = $this->createMeasListFromUnitsData($base_values);

    /** @var Drupal\unitsapi\Measurement $derived_measurement */
    $derived_measurement = $this->createDerivedMeasurementFromMeas($base_measurements, $to_unit);

    if ($log_input) {
      $derived_measurement->logInput($base_measurements);
    }

    return $derived_measurement;
  }

  /**
   * {@inheritdoc}
   */
  public function createDerivedMeasurementFromMeas(array $base_measurements, $to_unit) {
    /** @var Drupal\unitsapi\Measurement $derived_measurement */
    $derived_measurement = $this->measurementManager->createMeasurement($to_unit);

    /** @var Drupal\unitsapi\Plugin\UnitsApiPropertyInterface $unit_property */
    $unit_property = $derived_measurement->getUnitProperty();

    foreach ($base_measurements as $measurement) {
      $unit_property->addBaseMeasurement($measurement);
    }

    $unit_property->hydrateDerivedMeasurement($derived_measurement);

    return $derived_measurement;
  }

  /**
   * {@inheritdoc}
   */
  public function calcBaseMeasurement($property_quantity, $property_unit, array $base_values, $to_unit = NULL, $log_input = FALSE) {
    /** @var Drupal\unitsapi\Measurement $derived_measurement */
    $derived_measurement = $this->measurementManager->createMeasurement(
      $property_unit,
      $property_quantity
    );

    $derived_measurement = $this->convertMeasurementUnit(
      $derived_measurement,
      $derived_measurement->getDefaultUnit()
    );

    /** @var Drupal\unitsapi\Measurement[] $base_measurements */
    $base_measurements = $this->createMeasListFromUnitsData($base_values);

    /** @var Drupal\unitsapi\Measurement $return_measurement */
    $return_measurement = $this->calcBaseMeasurementFromMeas(
      $derived_measurement,
      $base_measurements,
      $to_unit
    );

    if ($log_input) {
      $return_measurement->logInput([
        '_base_measurements' => $base_measurements,
        '_derived_measurement' => $derived_measurement,
      ]);
    }

    return $return_measurement;
  }

  /**
   * {@inheritdoc}
   */
  public function calcBaseMeasurementFromMeas(MeasurementInterface $derived_measurement, array $base_measurements, $to_unit = NULL) {
    /** @var Drupal\unitsapi\Plugin\UnitsApiPropertyInterface $property */
    $derived_property = $derived_measurement->getUnitProperty();

    foreach ($base_measurements as $base_measurement) {
      $derived_property->addBaseMeasurement($base_measurement);
    }

    return $derived_property->getCalculatedBase($derived_measurement, $to_unit);
  }

  /**
   * Returns an array of measurements when given an array with the unit data.
   *
   * @param array $unit_data_list
   *   A multi-dimensional array with unit data.. The array keys of the top
   *   level should usually be left as integers, but may require a specific
   *   string in some rare circumstances.
   *   The second level keys are as follows:
   *   - unit: The unit ID of the unit.
   *   - quantity: The quantity of said unit.
   *
   * @todo: Improve docs on when the top level keys of $unit_data_list  should
   * contain strings.
   *
   * @return \Drupal\unitsapi\Measurement[]
   *   An array of Measurement objects containing the requested data.
   */
  protected function createMeasListFromUnitsData(array $unit_data_list) {
    $measurement_list = [];
    foreach ($unit_data_list as $key => $unit_data) {
      /** @var Drupal\unitsapi\Measurement $measurement */
      $measurement = $this->measurementManager->createMeasurement(
        $unit_data['unit'],
        $unit_data['quantity']
      );

      // The key should be set to the property id by default, but there may be
      // reason to differentiate different sets of the same property type.
      if (is_int($key)) {
        $key = $measurement->getUnitProperty()->getPluginId();
      }
      $measurement->setMeasurementKey($key);

      $measurement_list[] = $measurement;
    }

    return $measurement_list;
  }

  /**
   * {@inheritdoc}
   */
  public function createMeasurement($unit, $quantity = NULL) {
    return $this->measurementManager->createMeasurement($unit, $quantity);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateMeasurementFromUnits(array $unit_data_list, $return_type = NULL) {
    return $this->calculateMeasurementFromMeasurements(
      $this->createMeasListFromUnitsData($unit_data_list)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function calculateMeasurementFromMeasurements(array $measurement_list, $return_type = NULL) {
    $known_value_types = [];
    foreach ($measurement_list as $measurement) {
      $known_value_types[] = [
        'property' => $measurement->getPropertyId(),
        'key' => $measurement->getMeasurementKey(),
      ];
    }

    /** @var Drupal\unitsapi\Measurement[] $return_measurements */
    $return_measurements = [];
    $missing_info_list = $this->determineMissingMeasurement($known_value_types, $return_type);
    foreach ($missing_info_list as $missing_info) {
      // Missing info is derived. Used the measurements we have to create the
      // derived measurement.
      if ($missing_info['calculation_type'] === 'derived') {
        $to_unit = $this->propertyManager->getDefinition($missing_info['derived_property'])['defaultUnit'];
        /** @var Drupal\unitsapi\Measurement $derived_measurement */
        $derived_measurement = $this->createDerivedMeasurementFromMeas($measurement_list, $to_unit);
        $return_measurements[$derived_measurement->getPropertyId()] = $derived_measurement;
        continue;
      }

      // Missing info is a base value. Determine which property is derived and
      // use the API to calculate the missing base measurement.
      foreach ($measurement_list as $key => $measurement) {
        if ($measurement->getPropertyId() === $missing_info['derived_property']) {
          unset($measurement_list[$key]);
          /** @var Drupal\unitsapi\Measurement $derived_measurement */
          $derived_measurement = $measurement;
          break;
        }
      }
      /** @var Drupal\unitsapi\Measurement $missing_measurement */
      $missing_measurement = $this->calcBaseMeasurementFromMeas($derived_measurement, $measurement_list);
      $return_measurements[$missing_measurement->getPropertyId()] = $missing_measurement;
    }

    return $return_measurements;
  }

  /**
   * Returns info what sort of data is to be calculated.
   *
   * @param array $known_value_types
   *   An array of known values types. Top level is sequential and the second
   *   level contains 'key' and 'property'.
   * @param string|null $return_type
   *   If set to a plugin property ID, will only perform calculations for that
   *   property type. If left NULL, it may return more than one result.
   *
   * @return array
   *   A multidimensional array containind data on the determined missing
   *   measurements. The top level is sequential and the second level keys are:
   *   - derived_property: The property plugin ID of the derived property.
   *   - calculation_type: whether the missing value is 'derived' or a 'base'.
   */
  protected function determineMissingMeasurement(array $known_value_types, $return_type) {
    foreach ($this->propertyManager->getDerivedPropertyPieces() as $property_id => $formula_list) {
      foreach ($formula_list as $formula) {
        if (count($known_value_types) !== (count($formula) - 1)) {
          continue;
        }

        if (!$missing_element = $this->findMissingElement($formula, $known_value_types)) {
          continue;
        }

        if ($return_type && $missing_element['key'] != $return_type) {
          continue;
        }

        $missing_measurements[] = [
          'derived_property' => $property_id,
          'calculation_type' => $missing_element['key'] === $property_id ? 'derived' : 'base',
        ];
      }
    }
    return $missing_measurements;
  }

  /**
   * Determines if the known values fit a given formula.
   *
   * Does this by making sure that the $known_value_types param contains every
   * element in the $formula except one.
   *
   * @param array $formula
   *   A formula for a derived property that includes the derived property.
   * @param array $known_value_types
   *   An array of known values types. Top level is sequential and the second
   *   level contains 'key' and 'property'.
   *
   * @return array|bool
   *   If the formula does not align with the known value types, return FALSE.
   *   Otherwise, return an array with the 'key' and 'property' of the element
   *   missing from the formula.
   */
  protected static function findMissingElement(array $formula, array $known_value_types) {
    $found = [];
    foreach ($formula as $key => &$values) {
      foreach ($known_value_types as $k2 => &$v2) {
        if ($values['key'] === $v2['key'] && $values['property'] === $v2['property']) {
          unset($known_value_types[$k2]);
          $found[] = $key;
          break;
        }
      }
    }

    $diff_keys = array_values(array_diff(array_keys($formula), $found));

    if (count($diff_keys) === 1) {
      return $formula[$diff_keys[0]];
    }

    return FALSE;
  }

}
