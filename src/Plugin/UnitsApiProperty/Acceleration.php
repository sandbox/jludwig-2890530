<?php

namespace Drupal\unitsapi\Plugin\UnitsApiProperty;

use Drupal\unitsapi\Plugin\UnitsApiDerivedPropertyBase;

/**
 * Provides the Acceleration UnitsAPI property.
 *
 * @UnitsApiProperty(
 *   id = "acceleration",
 *   label = @Translation("Meter per second squared"),
 *   defaultUnit = "meter_per_second_squared",
 *   baseMeasurements = {
 *     "length" = {
 *       "unit" = "meter"
 *     },
 *     "time" = {
 *       "unit" = "second"
 *     }
 *   }
 * )
 */
class Acceleration extends UnitsApiDerivedPropertyBase {

  /**
   * {@inheritdoc}
   */
  protected function calcDerivedQuantity() {
    $length = $this->getBaseQuantity('length');
    $time = $this->getBaseQuantity('time');
    if (empty($time)) {
      return NULL;
    }
    return $length / ($time * $time);
  }

  /**
   * {@inheritdoc}
   */
  protected function calcBaseMeasurement($quantity) {
    if (isset($this->requiredMeasurements['length'])) {
      $time = $this->getBaseQuantity('time');
      return $quantity * $time * $time;
    }

    if (isset($this->requiredMeasurements['time'])) {
      $length = $this->getBaseQuantity('length');
      if (empty($quantity)) {
        return NULL;
      }
      return sqrt($length / $quantity);
    }
  }

}
