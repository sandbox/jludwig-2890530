<?php

namespace Drupal\Tests\unitsapi\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests behavior of ChemicalPhysicalProperty field.
 *
 * @group unitsapi
 */
class ConversionTests extends KernelTestBase {

  /**
   * Current logged in user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The UnitsAPI converter service.
   *
   * @var \Drupal\unitsapi\Converter
   */
  protected $converter;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['unitsapi'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->converter = \Drupal::service('unitsapi.converter');
  }

  /**
   * Check to see if an unit conversion equals an expected value.
   *
   * @param int|float $value
   *   A number containing the value of the measurement.
   * @param string $from_unit
   *   A string containing the name of the measurement to convert from.
   * @param string $to_unit
   *   A string containing the name of the measurement to convert to.
   * @param int|float $expected
   *   A number containing the expected result of the conversion.
   */
  protected function assertUnitConversion($value, $from_unit, $to_unit, $expected) {
    $test = $this->converter->convertUnit($value, $from_unit, $to_unit);
    $this->assertEquals($expected, round($test->getQuantity(), 6));
  }

  /**
   * Check to see if conversions to derived poperties work.
   *
   * @param array $base_values
   *   The base values used to make the derived property.
   * @param string $to_unit
   *   A string containing the name of the measurement to convert to.
   * @param int|float $expected
   *   A number containing the expected result of the conversion.
   */
  protected function assertDerivedConversion(array $base_values, $to_unit, $expected) {
    $test = $this->converter->createDerivedMeasurement($base_values, $to_unit);
    $this->assertEquals($expected, round($test->getQuantity(), 6));
  }

  /**
   * Checks to see if computed a missing base value a derived property works.
   *
   * @param int|float $derived_quantity
   *   The derived property quantity.
   * @param string $derived_unit
   *   The derived quantity unit plugin ID.
   * @param array $known_base_values
   *   The base values (except one) used to make the derived property.
   * @param string $to_unit
   *   A string containing the name of the measurement to convert to.
   * @param int|float $expected
   *   A number containing the expected result of the conversion.
   */
  protected function assertBaseMeasurementCalc($derived_quantity, $derived_unit, array $known_base_values, $to_unit, $expected) {
    $test = $this->converter->calcBaseMeasurement(
      $derived_quantity,
      $derived_unit,
      $known_base_values,
      $to_unit
    );
    $this->assertEquals($expected, round($test->getQuantity(), 6));
  }

  /**
   * Tests various length unit conversions.
   */
  public function testUnitsApiLength() {
    $this->assertUnitConversion(4000, 'millimeter', 'inch', 157.480315);
    $this->assertUnitConversion(120, 'centimeter', 'foot', 3.937008);
    $this->assertUnitConversion(92, 'decimeter', 'yard', 10.061242);
    $this->assertUnitConversion(5, 'meter', 'mile', 0.003107);
    $this->assertUnitConversion(1.5, 'kilometer', 'foot', 4921.259843);
    $this->assertUnitConversion(30, 'foot', 'yard', 10);
    $this->assertUnitConversion(87, 'inch', 'decimeter', 22.098);
    $this->assertUnitConversion(135, 'yard', 'meter', 123.444);
    $this->assertUnitConversion(10, 'mile', 'yard', 17600);
    $this->assertUnitConversion(22, 'mile', 'kilometer', 35.405568);
  }

  /**
   * Tests various volume unit conversions.
   */
  public function testUnitsApiVolume() {
    $this->assertUnitConversion(90, 'cubic_inch', 'us_liquid_quart', 1.558441);
    $this->assertUnitConversion(4, 'cubic_yard', 'cubic_foot', 107.999993);
    $this->assertUnitConversion(16, 'cup', 'imperial_pint', 6.661392);
    $this->assertUnitConversion(40, 'ounce_us_fluid', 'cubic_inch', 72.187519);
    $this->assertUnitConversion(25, 'imperial_pint', 'tablespoon', 960.760251);
    $this->assertUnitConversion(70, 'us_dry_quart', 'imperial_gallon', 16.956433);
    $this->assertUnitConversion(55, 'tablespoon', 'cubic_inch', 49.628902);
    $this->assertUnitConversion(120, 'teaspoon', 'cup', 2.500001);
    $this->assertUnitConversion(3, 'cup', 'milliliter', 709.7646);
    $this->assertUnitConversion(2, 'liter', 'ounce_imperial_fluid', 70.390166);
    $this->assertUnitConversion(12, 'us_gallon', 'liter', 45.424944);
    $this->assertUnitConversion(2, 'cubic_foot', 'cup', 239.376689);
  }

  /**
   * Tests various mass unit conversions.
   */
  public function testUnitsApiMass() {
    $this->assertUnitConversion(25, 'milligram', 'carat', 0.125);
    $this->assertUnitConversion(60, 'grain', 'gram', 3.887935);
    $this->assertUnitConversion(122, 'pennyweight', 'slug', 0.013001);
    $this->assertUnitConversion(812, 'pound', 'long_ton', 0.3625);
    $this->assertUnitConversion(88, 'kilogram', 'short_ton', 0.097003);
    $this->assertUnitConversion(50, 'stone', 'pound', 699.999954);
  }

  /**
   * Tests various area unit conversions.
   */
  public function testUnitsApiArea() {
    $this->assertUnitConversion(197, 'square_yard', 'acre', 0.040702);
    $this->assertUnitConversion(70, 'hectare', 'square_mile', 0.270272);
    $this->assertUnitConversion(103, 'square_inch', 'square_foot', 0.715278);
    $this->assertUnitConversion(76, 'are', 'acre', 1.877993);
    $this->assertUnitConversion(9, 'square_meter', 'square_yard', 10.76391);
    $this->assertUnitConversion(38, 'square_kilometer', 'square_mile', 14.671883);
    $this->assertUnitConversion(10, 'aankadam', 'kottah', 1.000149);
    $this->assertUnitConversion(20, 'perch', 'rood', 0.499946);
    $this->assertUnitConversion(25, 'cent', 'acre', 0.250008);
    $this->assertUnitConversion(12, 'chatak', 'aankadam', 74.995516);
    $this->assertUnitConversion(121, 'kottah', 'acre', 1.999986);
    $this->assertUnitConversion(5, 'guntha', 'perch', 20.001977);
    $this->assertUnitConversion(90, 'ground', 'kanal', 2.000024);
    $this->assertUnitConversion(4, 'marla', 'square_yard', 2400.017031);
    $this->assertUnitConversion(3, 'rood', 'acre', 0.749994);
    $this->assertUnitConversion(10, 'bigha_I', 'cent', 399.985174);
    $this->assertUnitConversion(10, 'bigha_II', 'acre', 6.249986);
    $this->assertUnitConversion(2, 'kanal', 'chatak', 479.958383);
    $this->assertUnitConversion(3, 'biswa_I', 'acre', 23.999901);
    $this->assertUnitConversion(2, 'biswa_II', 'acre', 24.9999);
  }

  /**
   * Tests various pressure unit conversions.
   */
  public function testUnitsApiPressure() {
    $this->assertUnitConversion(5926, 'pascal', 'torr', 44.448645);
    $this->assertUnitConversion(28, 'bar', 'psi', 406.105683);
    $this->assertUnitConversion(865, 'torr', 'bar', 1.153239);
    $this->assertUnitConversion(55, 'millibar', 'pascal', 5500);
  }

  /**
   * Tests various temperature unit conversions.
   */
  public function testUnitsApiTemperature() {
    $this->assertUnitConversion(25, 'celsius', 'fahrenheit', 77);
    $this->assertUnitConversion(45, 'celsius', 'kelvin', 318.15);
    $this->assertUnitConversion(90, 'fahrenheit', 'celsius', 32.222222);
    $this->assertUnitConversion(55, 'fahrenheit', 'kelvin', 285.927778);
    $this->assertUnitConversion(295, 'kelvin', 'celsius', 21.85);
    $this->assertUnitConversion(300, 'kelvin', 'fahrenheit', 80.33);
  }

  /**
   * Tests various time unit conversions.
   */
  public function testUnitsApiTime() {
    $this->assertUnitConversion(82, 'hour', 'day', 3.416667);
    $this->assertUnitConversion(3.5, 'day', 'minute', 5040);
    $this->assertUnitConversion(20, 'year', 'hour', 175200);
  }

  /**
   * Tests various cross-unit property conversions.
   */
  public function testDerivedConversions() {
    $test_values = [
      'acceleration' => [
        'base_values' => [
          ['unit' => 'kilometer', 'quantity' => 103680],
          ['unit' => 'minute', 'quantity' => 2.4],
        ],
        'derived_property' => [
          'unit' => 'meter_per_second_squared',
          'quantity' => 5000,
        ],
      ],
      'area' => [
        'base_values' => [
          ['unit' => 'centimeter', 'quantity' => 25.25],
          ['unit' => 'kilometer', 'quantity' => .005],
        ],
        'derived_property' => [
          'unit' => 'square_meter',
          'quantity' => 1.2625,
        ],
      ],
      'density' => [
        'base_values' => [
          ['unit' => 'liter', 'quantity' => 40],
          ['unit' => 'kilogram', 'quantity' => 1400],
        ],
        'derived_property' => [
          'unit' => 'gram_per_cubic_centimeter',
          'quantity' => 35,
        ],
      ],
      'force' => [
        'base_values' => [
          ['unit' => 'metric_ton', 'quantity' => .25],
          ['unit' => 'gal', 'quantity' => .04],
        ],
        'derived_property' => [
          'unit' => 'dyne',
          'quantity' => 10000,
        ],
      ],
      'pressure' => [
        'base_values' => [
          ['unit' => 'newton', 'quantity' => 2500],
          ['unit' => 'square_meter', 'quantity' => 2.5],
        ],
        'derived_property' => [
          'unit' => 'millibar',
          'quantity' => 10,
        ],
      ],
      'volume' => [
        'base_values' => [
          ['unit' => 'meter', 'quantity' => 20],
          ['unit' => 'square_meter', 'quantity' => 4],
        ],
        'derived_property' => [
          'unit' => 'liter',
          'quantity' => 80000,
        ],
      ],
    ];

    foreach ($test_values as $unit_data) {
      $this->assertDerivedConversion(
        $unit_data['base_values'],
        $unit_data['derived_property']['unit'],
        $unit_data['derived_property']['quantity']
      );

      $this->assertBaseMeasurementCalc(
        $unit_data['derived_property']['quantity'],
        $unit_data['derived_property']['unit'],
        [$unit_data['base_values'][0]],
        $unit_data['base_values'][1]['unit'],
        $unit_data['base_values'][1]['quantity']
      );

      $this->assertBaseMeasurementCalc(
        $unit_data['derived_property']['quantity'],
        $unit_data['derived_property']['unit'],
        [$unit_data['base_values'][1]],
        $unit_data['base_values'][0]['unit'],
        $unit_data['base_values'][0]['quantity']
      );
    }
  }

}
