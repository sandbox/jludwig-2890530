<?php

namespace Drupal\unitsapi;

/**
 * UnitsAPI Measurement Interface definition.
 *
 * @package Drupal\unitsapi
 */
interface MeasurementInterface {

  const FORMAT_QUANTITY = 1;
  const FORMAT_SYMBOL = 2;
  const FORMAT_TEXT = 3;
  const FORMAT_SYMBOL_ONLY = 4;
  const FORMAT_TEXT_ONLY = 5;

  /**
   * Converts measurement to a given unit.
   *
   * @param string $to_unit
   *   The unit plugin ID to convert to.
   *
   * @throws Drupal\unitsapi\Exception\ConversionException
   *   Thrown if a problem with the conversion is encountered.
   */
  public function toUnit($to_unit);

  /**
   * Sets the quantity for the measurement.
   *
   * @param int|float $quantity
   *   The quantity to set.
   *
   * @return $this
   */
  public function setQuantity($quantity);

  /**
   * Returns the quantity of the measurement.
   *
   * @return int|float|null
   *   The measurement quantity.
   */
  public function getQuantity();

  /**
   * Sets the key for the measurement.
   *
   * @param string $key
   *   An identifier for this measurement for use in base values for derived
   *   properties etc.
   *
   * @return $this
   */
  public function setMeasurementKey($key);

  /**
   * Returns the key for the measurement.
   *
   * @return string
   *   An identifying string for this measurement.
   */
  public function getMeasurementKey();

  /**
   * Logs an array to the measurement.
   *
   * This will usually be input measurement data.
   *
   * @param array $input
   *   The inputs to log.
   */
  public function logInput(array $input);

  /**
   * Returns the input log.
   *
   * @return array
   *   The input log of the measurement.
   */
  public function getLoggedInput();

  /**
   * Returns the unit property object of the Measurement.
   *
   * @return \Drupal\unitsapi\Plugin\UnitsApiPropertyInterface
   *   The unit property of the measurement.
   */
  public function getUnitProperty();

  /**
   * Returns the plugin ID of the unit property of the measurement.
   *
   * @return string
   *   The unit property ID of the measurement.
   */
  public function getPropertyId();

  /**
   * Returns the unit plugin definition of the unit of the measurement.
   *
   * @return array
   *   The unit plugin definition of this measurement.
   */
  public function getUnit();

  /**
   * Returns the plugin ID of the unit of the measurement.
   *
   * @return string
   *   The unit plugin ID of this measurement.
   */
  public function getUnitId();

  /**
   * Returns the default unit for the unit property of the measurement.
   *
   * @return string
   *   The default unit of the property of this measurement.
   */
  public function getDefaultUnit();

  /**
   * Returns the conversion factor of the unit of the measurement.
   *
   * @return float
   *   The conversion factor of the unit of this measurement.
   */
  public function getFactor();

  /**
   * Returns the unit symbol for this measurement..
   *
   * @return string
   *   The unit symbol of the measurement.
   */
  public function getUnitSymbol();

  /**
   * Returns the full human readable text of the measurement.
   *
   * Checks the quantity of the measurement and returns the singular unit string
   * if the quantity is exactly 1 and the plural string otherwise.
   *
   * @return string
   *   The human readable text of the measurement.
   */
  public function getUnitString();

  /**
   * Returns the a formatted representation of the measurement.
   *
   * @param int|null $format
   *   The format to return. If left NULL, defaults to quantity only.
   *   - \Drupal\unitsapi\MeasurementInterface\FORMAT_QUANTITY: Returns only the
   *     quantity; basically the same as getQuantity().
   *   - \Drupal\unitsapi\MeasurementInterface\FORMAT_SYMBOL: Returns the
   *     quantity and the unit symbol, or the text if no symbol exists.
   *   - \Drupal\unitsapi\MeasurementInterface\FORMAT_TEXT: Returns the quantity
   *     and the full human string representing the unit.
   *
   * @return int|float|string
   *   The formatted representation of the measurement.
   */
  public function getFormatted($format = NULL);

  /**
   * Sets the number format info for when getFormatted() is called.
   *
   * @param int $precision
   *   The precision to send to number_format().
   * @param string $decimal
   *   The decimal to send to number_format().
   * @param string $separator
   *   The thousands separator to send to number_format().
   *
   * @return $this
   */
  public function setNumberFormat($precision = 6, $decimal = '.', $separator = ',');

}
