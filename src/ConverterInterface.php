<?php

namespace Drupal\unitsapi;

/**
 * Interface for the UnitsAPI Converter service.
 *
 * @package Drupal\unitsapi
 */
interface ConverterInterface {

  /**
   * Converts a unit to another unit of the same type.
   *
   * @param int|float $quantity
   *   A number containing the value of the measurement.
   * @param string $from_unit
   *   A string containing the name of the unit to convert from.
   * @param string $to_unit
   *   A string containing the id of the unit to convert to.
   * @param bool $log_input
   *   A boolean indicating whether to log the measurement created from the
   *   input data to the returned measurement.
   *
   * @return \Drupal\unitsapi\Measurement
   *   A Measurement object representing the converted unit.
   */
  public function convertUnit($quantity, $from_unit, $to_unit, $log_input = FALSE);

  /**
   * Converts a measurement object to a given unit.
   *
   * @param \Drupal\unitsapi\Measurement $from_measurement
   *   The measurement object to convert.
   * @param string $to_unit
   *   A string containing the id of the unit to convert to.
   *
   * @return \Drupal\unitsapi\Measurement
   *   A Measurement object representing the converted unit.
   */
  public function convertMeasurementUnit(MeasurementInterface $from_measurement, $to_unit);

  /**
   * Returns a derived property measurement from a list of base measurements.
   *
   * @param array $base_values
   *   A multi-dimensional array with the base values needed to calculated the
   *   values of the derived property measurement. The array keys of the top
   *   level should usually be left as integers, but may require a specific
   *   string in some rare circumstances.
   *   The second level keys are as follows:
   *   - unit: The unit ID of the unit.
   *   - quantity: The quantity of said unit.
   * @param string $to_unit
   *   A string containing the id of the unit to convert to.
   * @param bool $log_input
   *   A boolean indicating whether to log the measurements created from the
   *   input data to the returned measurement.
   *
   * @todo: Improve docs on when the top level keys of $base_values  should
   * contain strings.
   *
   * @return \Drupal\unitsapi\Measurement
   *   A Measurement object containing the requested data.
   */
  public function createDerivedMeasurement(array $base_values, $to_unit, $log_input = FALSE);

  /**
   * Returns a derived property measurement from a list of base measurements.
   *
   * @param \Drupal\unitsapi\Measurement[] $base_measurements
   *   An array of measurements used to build the derived property measurement.
   * @param string $to_unit
   *   A string containing the id of the unit to convert to.
   *
   * @return \Drupal\unitsapi\Measurement
   *   A Measurement object containing the requested data.
   */
  public function createDerivedMeasurementFromMeas(array $base_measurements, $to_unit);

  /**
   * Returns a measurement calculated from a derived property and base values.
   *
   * @param mixed $property_quantity
   *   The quantity of the derived property.
   * @param mixed $property_unit
   *   The unit of the derived quantity.
   * @param array $base_values
   *   A multi-dimensional array containing all the base values needed to create
   *   the derived property except for one; the values of the returned,
   *   calculated measurement. The array keys of the top level should usually be
   *   left as ints, but may require a specific string in rare circumstances.
   *   The second level keys are as follows:
   *   - unit: The unit ID of the unit.
   *   - quantity: The quantity of said unit.
   * @param string|null $to_unit
   *   A string containing the id of the unit to convert to. Returns the default
   *   property unit param is left NULL.
   * @param bool $log_input
   *   A boolean indicating whether to log the measurement created from the
   *   input data to the returned measurement.
   *
   * @todo: Improve docs on when the top level keys of $base_values  should
   * contain strings.
   *
   * @return \Drupal\unitsapi\Measurement
   *   A Measurement object containing the requested data.
   */
  public function calcBaseMeasurement($property_quantity, $property_unit, array $base_values, $to_unit = NULL, $log_input = FALSE);

  /**
   * Calculates measurement from a derived measurement and base measurements.
   *
   * @param Measurement $derived_measurement
   *   A measurement object for the derived property.
   * @param \Drupal\unitsapi\Measurement[] $base_measurements
   *   An array of measurement objects representing the known base values of the
   *   derived property.
   * @param string|null $to_unit
   *   A string containing the id of the unit to convert to. Returns the default
   *   property unit param is left NULL.
   *
   * @return \Drupal\unitsapi\Measurement
   *   A Measurement object containing the requested data.
   */
  public function calcBaseMeasurementFromMeas(MeasurementInterface $derived_measurement, array $base_measurements, $to_unit = NULL);

  /**
   * Shortcut for \Drupal\unitsapi\MeasurementManager::createMeasurement().
   *
   * @param string $unit
   *   The plugin id of the unit to create a measurement for.
   * @param int|float|null $quantity
   *   The quantity of the measurement.
   *
   * @return \Drupal\unitsapi\MeasurementInterface
   *   The created measurement object.
   */
  public function createMeasurement($unit, $quantity = NULL);

  /**
   * Returns an array of measurements that can be computed from input unit data.
   *
   * @param array $unit_data_list
   *   A multi-dimensional array with unit data.. The array keys of the top
   *   level should usually be left as integers, but may require a specific
   *   string in some rare circumstances.
   *   The second level keys are as follows:
   *   - unit: The unit ID of the unit.
   *   - quantity: The quantity of said unit.
   * @param string|null $return_type
   *   If set to a plugin property ID, will only perform calculations for that
   *   property type. If left NULL, it may return more than one result.
   *
   * @return \Drupal\unitsapi\Measurement[]
   *   An array of Measurement objects containing the requested data, keyed by
   *   the property plugin ID.
   */
  public function calculateMeasurementFromUnits(array $unit_data_list, $return_type = NULL);

  /**
   * Returns an array of measurements that computed from input measurements.
   *
   * @param \Drupal\unitsapi\Measurement[] $measurement_list
   *   An array of measurements representing the input.
   * @param string|null $return_type
   *   If set to a plugin property ID, will only perform calculations for that
   *   property type. If left NULL, it may return more than one result.
   *
   * @return \Drupal\unitsapi\Measurement[]
   *   An array of Measurement objects containing the requested data, keyed by
   *   the property plugin ID.
   */
  public function calculateMeasurementFromMeasurements(array $measurement_list, $return_type = NULL);

}
