<?php

namespace Drupal\unitsapi\Plugin\UnitsApiProperty;

use Drupal\unitsapi\Plugin\UnitsApiPropertyBase;

/**
 * Provides the Time UnitsAPI property.
 *
 * @UnitsApiProperty(
 *   id = "time",
 *   label = @Translation("Time"),
 *   defaultUnit = "second"
 * )
 */
class Time extends UnitsApiPropertyBase {
}
