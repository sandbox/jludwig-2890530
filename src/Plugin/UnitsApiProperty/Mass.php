<?php

namespace Drupal\unitsapi\Plugin\UnitsApiProperty;

use Drupal\unitsapi\Plugin\UnitsApiPropertyBase;

/**
 * Provides the Mass UnitsAPI property.
 *
 * @UnitsApiProperty(
 *   id = "mass",
 *   label = @Translation("Mass"),
 *   defaultUnit = "kilogram"
 * )
 */
class Mass extends UnitsApiPropertyBase {
}
