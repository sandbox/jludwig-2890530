<?php

namespace Drupal\unitsapi\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\unitsapi\Measurement;

/**
 * Defines an interface for Units API property plugins.
 */
interface UnitsApiPropertyInterface extends PluginInspectionInterface {

  /**
   * Convert one class to another.
   *
   * @param \Drupal\unitsapi\Measurement $from_measurement
   *   The measurement object to convert from. Must include a quantity.
   * @param string $to_unit
   *   A string containing the id of the unit to convert to.
   *
   * @return \Drupal\unitsapi\Measurement
   *   The converted measurement.
   *
   * @throws \Drupal\unitsapi\Exception\ConversionException
   *   Thrown if a problem with the conversion is encountered.
   */
  public function convertUnit(Measurement $from_measurement, $to_unit);

  /**
   * Returns the default unit plugin id for the property.
   *
   * @return string
   *   The default unit plugin id for the property.
   */
  public function getDefaultUnit();

}
