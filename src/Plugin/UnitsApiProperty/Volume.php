<?php

namespace Drupal\unitsapi\Plugin\UnitsApiProperty;

use Drupal\unitsapi\Plugin\UnitsApiDerivedPropertyBase;

/**
 * Provides the Volume UnitsAPI property.
 *
 * @UnitsApiProperty(
 *   id = "volume",
 *   label = @Translation("Volume"),
 *   defaultUnit = "cubic_meter",
 *   baseMeasurements = {
 *     "area" = {
 *       "unit" = "square_meter"
 *     },
 *     "length" = {
 *       "unit" = "meter"
 *     }
 *   }
 * )
 */
class Volume extends UnitsApiDerivedPropertyBase {

  /**
   * {@inheritdoc}
   */
  protected function calcDerivedQuantity() {
    $area = $this->getBaseQuantity('area');
    $length = $this->getBaseQuantity('length');
    return $area * $length;
  }

  /**
   * {@inheritdoc}
   */
  protected function calcBaseMeasurement($quantity) {
    if (isset($this->requiredMeasurements['area'])) {
      $length = $this->getBaseQuantity('length');
      if (empty($length)) {
        return NULL;
      }
      return $quantity / $length;
    }

    if (isset($this->requiredMeasurements['length'])) {
      $area = $this->getBaseQuantity('area');
      if (empty($area)) {
        return NULL;
      }
      return $quantity / $area;
    }
  }

}
