<?php

namespace Drupal\unitsapi;

use Drupal\unitsapi\Plugin\UnitsApiPropertyManager;

/**
 * Defines the measurement manager service.
 *
 * This class facilitates the creation of measurements.
 *
 * @package Drupal\unitsapi
 */
class MeasurementManager {

  /**
   * Drupal\unitsapi\UnitsApiUnitManager definition.
   *
   * @var \Drupal\unitsapi\UnitsApiUnitManager
   */
  protected $unitsManager;

  /**
   * Drupal\unitsapi\Plugin\UnitsApiPropertyManager definition.
   *
   * @var \Drupal\unitsapi\Plugin\UnitsApiPropertyManager
   */
  protected $unitPropertyManager;

  /**
   * Constructs a new Measurement object.
   *
   * @param \Drupal\unitsapi\UnitsApiUnitManager $units_manager
   *   The UnitsAPI unit manager.
   * @param \Drupal\unitsapi\UnitsApiPropertyManager $property_manager
   *   The UnitsAPI property manager.
   */
  public function __construct(UnitsApiUnitManager $units_manager, UnitsApiPropertyManager $property_manager) {
    $this->unitsManager = $units_manager;
    $this->unitPropertyManager = $property_manager;
  }

  /**
   * Creates a new measurement.
   *
   * @param string $unit
   *   The plugin id of the unit to create a measurement for.
   * @param int|float|null $quantity
   *   The quantity of the measurement.
   *
   * @return \Drupal\unitsapi\MeasurementInterface
   *   The created measurement object.
   *
   * @todo verify unit definition exists.
   */
  public function createMeasurement($unit, $quantity = NULL) {
    $unit_definition = $this->unitsManager->getDefinition($unit);

    /** @var Drupal\unitsapi\Plugin\UnitsApiPropertyInterface $property */
    $property = $this->unitPropertyManager
      ->createInstance($unit_definition['unit_property']);

    return new Measurement($property, $unit_definition, $quantity);
  }

}
