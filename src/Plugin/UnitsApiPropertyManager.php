<?php

namespace Drupal\unitsapi\Plugin;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides the Units API property plugin manager.
 */
class UnitsApiPropertyManager extends DefaultPluginManager {

  /**
   * Contains the sets of measurement data needed to make derived property.
   *
   * @var array
   */
  protected $derivedPropertyPieces = [];

  /**
   * Constructs a new UnitsApiPropertyManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/UnitsApiProperty', $namespaces, $module_handler, 'Drupal\unitsapi\Plugin\UnitsApiPropertyInterface', 'Drupal\unitsapi\Annotation\UnitsApiProperty');

    $this->alterInfo('unitsapi_unitsapi_property_info');
    $this->setCacheBackend($cache_backend, 'unitsapi_unitsapi_property_plugins');
  }

  /**
   * {@inheritdoc}
   *
   * @todo ensure that derived properties have base measurements and that
   * properties with base measurements are derived.
   * @todo validate max depth.
   * @todo validate default unit.
   */
  public function processDefinition(&$definition, $plugin_id) {
    parent::processDefinition($definition, $plugin_id);
    if (isset($definition['baseMeasurements'])) {
      $this->processBaseMeasurements($definition, $plugin_id);
    }
  }

  /**
   * Processes the base measurements section of the plugin definition.
   *
   * @param array &$definition
   *   An array of plugin definitions (empty array if no definitions were
   *   found). Keys are plugin IDs.
   * @param string $plugin_id
   *   The plugin id being processed.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   Thrown if baseMeasurements isn't an array.
   */
  protected function processBaseMeasurements(array &$definition, $plugin_id) {
    if (!is_array($definition['baseMeasurements'])) {
      throw new PluginException(sprintf('Plugin (%s) definition "baseMeasurements" must be an array.', $plugin_id));
    }
    foreach ($definition['baseMeasurements'] as $key => $property_data) {
      $property = isset($property_data['property']) ? $property_data['property'] : $key;
      $count = isset($property_data['count']) ? $property_data['count'] : 1;
      $definition['baseMeasurements'][$key] = [
        'unit' => $property_data['unit'],
        'property' => $property,
        'count' => $count,
      ];
    }
  }

  /**
   * Returns the sets of measurement data needed to make derived properties.
   *
   * @return array
   *   The sets of measurement data needed to make derived properties.
   */
  public function getDerivedPropertyPieces() {
    if (count($this->derivedPropertyPieces)) {
      return $this->derivedPropertyPieces;
    }

    // Build the standard formulas for the derived properties.
    foreach ($this->getDefinitions() as $unit_property => $definition_data) {
      if (isset($definition_data['baseMeasurements'])) {
        $this->populateStandardFormula($unit_property);
      }
    }

    // Build alternative formulas based on derived base units.
    foreach ($this->derivedPropertyPieces as $unit_property => $property_formulas) {
      $this->buildAltPropertyFormulas($unit_property);
    }

    // Add the property itself to the mix.
    foreach ($this->derivedPropertyPieces as $unit_property => &$property_formulas) {
      foreach ($property_formulas as &$formula) {
        $formula[] = [
          'property' => $unit_property,
          'key' => $unit_property,
        ];
      }
    }

    return $this->derivedPropertyPieces;
  }

  /**
   * Builds the standard formulas for derived properties.
   *
   * @param string $unit_property
   *   The property plugin ID to build the standard formula for.
   */
  protected function populateStandardFormula($unit_property) {
    $base_measurements = $this->getDefinitions()[$unit_property]['baseMeasurements'];
    foreach ($base_measurements as $key => $property_data) {
      $property_data['key'] = $key;
      $count = $property_data['count'];
      unset($property_data['count']);
      unset($property_data['unit']);

      for ($i = 0; $i < $count; $i++) {
        $this->derivedPropertyPieces[$unit_property][0][] = $property_data;
      }
    }
  }

  /**
   * Builds additional formulas for derived properties.
   *
   * @param string $unit_property
   *   The property plugin ID to build the alt formulas for.
   */
  protected function buildAltPropertyFormulas($unit_property) {
    // Alts may have already been built if used in another derived property.
    if (count($this->derivedPropertyPieces[$unit_property]) > 1) {
      return;
    }

    foreach ($this->derivedPropertyPieces[$unit_property][0] as $base_measurement) {
      if (!isset($this->derivedPropertyPieces[$base_measurement['property']])) {
        continue;
      }

      // @todo it would be great to be able to handle this someday.
      if ($base_measurement['key'] !== $base_measurement['property']) {
        continue;
      }

      // If there's a deeper level, build the alt formulas there so they can be
      // added to this one.
      $this->buildAltPropertyFormulas($base_measurement['property']);

      $this->buildAltFormulasForBase($unit_property, $base_measurement['property']);
    }
  }

  /**
   * Builds alternative formulas based on derived base value formulas.
   *
   * @param string $derived_property
   *   The derived property that the formulas are being added to.
   * @param string $base_property
   *   The (derived) base property that has the formulas being added.
   */
  protected function buildAltFormulasForBase($derived_property, $base_property) {
    $property_definition = $this->getDefinition($derived_property);
    $max_depth = isset($property_definition['maxCalcDepth']) ? $property_definition['maxCalcDepth'] : 0;
    $i = 0;

    $alt_formulas = [];
    foreach ($this->derivedPropertyPieces[$derived_property] as $formula) {
      $alt_formulas = array_merge(
        $alt_formulas,
        $this->buildAltFormulasFromFormula($base_property, $formula)
      );

      $i++;
      if ($max_depth && $i >= $max_depth) {
        break;
      }

    }

    $this->derivedPropertyPieces[$derived_property] = array_merge(
      $this->derivedPropertyPieces[$derived_property],
      $alt_formulas
    );
  }

  /**
   * Builds out the additional formulas from a given formula.
   *
   * @param string $base_property
   *   The (derived) base property that has the formulas being added.
   * @param array $formula
   *   The formula to derived the alternate formulas from.
   *
   * @return array
   *   An array of formulas built from given formula.
   */
  protected function buildAltFormulasFromFormula($base_property, array $formula) {
    $formula_base = [];
    foreach ($formula as $base_measurement) {
      if ($base_measurement['property'] !== $base_property) {
        $formula_base[] = $base_measurement;
        continue;
      }
      $base_property_formulas = $this->derivedPropertyPieces[$base_property];
    }

    $alt_formulas = [];
    foreach ($base_property_formulas as $property_replacement) {
      $alt_formulas[] = array_merge($formula_base, $property_replacement);
    }

    return $alt_formulas;
  }

}
