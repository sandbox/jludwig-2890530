<?php

namespace Drupal\unitsapi\Plugin\UnitsApiProperty;

use Drupal\unitsapi\Plugin\UnitsApiDerivedPropertyBase;

/**
 * Provides the Force UnitsAPI property.
 *
 * @UnitsApiProperty(
 *   id = "force",
 *   label = @Translation("Force"),
 *   defaultUnit = "newton",
 *   baseMeasurements = {
 *     "mass" = {
 *       "unit" = "kilogram"
 *     },
 *     "acceleration" = {
 *       "unit" = "meter_per_second_squared"
 *     }
 *   }
 * )
 */
class Force extends UnitsApiDerivedPropertyBase {

  /**
   * {@inheritdoc}
   */
  protected function calcDerivedQuantity() {
    $mass = $this->getBaseQuantity('mass');
    $acceleration = $this->getBaseQuantity('acceleration');
    return $mass * $acceleration;
  }

  /**
   * {@inheritdoc}
   */
  protected function calcBaseMeasurement($quantity) {
    if (isset($this->requiredMeasurements['mass'])) {
      $acceleration = $this->getBaseQuantity('acceleration');
      if (empty($acceleration)) {
        return NULL;
      }
      return $quantity / $acceleration;
    }

    if (isset($this->requiredMeasurements['acceleration'])) {
      $mass = $this->getBaseQuantity('mass');
      if (empty($mass)) {
        return NULL;
      }
      return $quantity / $mass;
    }
  }

}
