<?php

namespace Drupal\unitsapi\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Units API property item annotation object.
 *
 * @see \Drupal\unitsapi\Plugin\UnitsApiPropertyManager
 * @see plugin_api
 *
 * @Annotation
 */
class UnitsApiProperty extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The default unit of the property.
   *
   * @var string
   */
  public $defaultUnit;

  /**
   * The base measurements needed to create a derived property.
   *
   * This is a multidimensional array with the the top level key being the "key"
   * expected in base measurements being added to the property. Generally, it
   * will simply be the unit property plugin id that is expected.
   *
   * The second level array keys are as follows:
   * - unit: (required) The plugin id of the expected unit.
   * - count: (optional) The number of this unit type expected. If omitted, a
   *   single unit of this type will be expected.
   * - property: (optional) The plugin id of the expected unit property. If
   *   omitted, the top level "key" will be the expected property.
   *
   * Note: The top level key must be a valid unit property plugin id if the
   * 'property' key is omitted in the second level.
   *
   * @var array
   */
  public $baseMeasurements;

  /**
   * Set to TRUE if this property requires custom factors for basic conversion.
   *
   * (Optional)
   *
   * @var bool
   */
  public $customFactors;

  /**
   * Specifies the max depth for automated calculation.
   *
   * (Optional)
   *
   * Some derived properties have conflicting base units of base units, and
   * one simply does not throw a bunch of measurements at a calculator and get
   * back a valid calculation. Example: length in pressure (derived from length
   * from acceleration and length from area).
   *
   * @var int
   */
  public $maxCalcDepth;

}
