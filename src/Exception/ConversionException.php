<?php

namespace Drupal\unitsapi\Exception;

/**
 * Exception thrown when a unit or measurement conversion cannot be executed.
 */
class ConversionException extends \Exception {

}
