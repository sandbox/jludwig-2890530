<?php

namespace Drupal\unitsapi;

use Drupal\unitsapi\Plugin\UnitsApiPropertyInterface;

/**
 * UnitsAPI Measurement class definition.
 *
 * @package Drupal\unitsapi
 */
class Measurement implements MeasurementInterface {

  /**
   * The unit property plugin instance.
   *
   * @var \Drupal\unitsapi\Plugin\UnitsApiPropertyInterface
   */
  protected $unitProperty;

  /**
   * The plugin definition of the unit.
   *
   * @var array
   */
  protected $unitDefinition;

  /**
   * The quantity of the unit in the measurement.
   *
   * @var int|float|null
   */
  protected $quantity;

  /**
   * A key associated with this measurement.
   *
   * This is only used for base values when creating derivative property
   * measurements or calculating a base value from a derivative property.
   *
   * @var string
   */
  protected $measurementKey;

  /**
   * A log of the input measurements.
   *
   * May be useful for debugging.
   *
   * @var array
   */
  protected $inputLog = [];

  /**
   * An array w/precision, decimal, and separator; called in getFormatted().
   *
   * @var array
   */
  protected $numberFormat = [];

  /**
   * Constructs a new Measurement object.
   *
   * @param \Drupal\unitsapi\Plugin\UnitsApiPropertyInterface $unit_property
   *   A unit property plugin.
   * @param array $unit_definition
   *   A unit plugin definition.
   * @param float|int|null $quantity
   *   The quantity of the measurement. May be left out or set to NULL to
   *   specify an unknown quantity.
   */
  public function __construct(UnitsApiPropertyInterface $unit_property, array $unit_definition, $quantity = NULL) {
    $this->unitProperty = $unit_property;
    $this->unitDefinition = $unit_definition;
    $this->quantity = $quantity;
  }

  /**
   * Return a string representing the Measurement.
   *
   * @return string
   *   The object string.
   */
  public function __toString() {
    return (string) $this->getFormatted();
  }

  /**
   * {@inheritdoc}
   */
  public function toUnit($to_unit) {
    $converted_meas = $this->unitProperty->convertUnit($this, $to_unit);
    $this->setQuantity($converted_meas->getQuantity());
    $this->unitProperty = $converted_meas->getUnitProperty();
    $this->unitDefinition = $converted_meas->getUnit();
  }

  /**
   * {@inheritdoc}
   */
  public function setQuantity($quantity) {
    $this->quantity = $quantity;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuantity() {
    return $this->quantity;
  }

  /**
   * {@inheritdoc}
   */
  public function setMeasurementKey($key) {
    $this->measurementKey = $key;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMeasurementKey() {
    if (isset($this->measurementKey)) {
      return $this->measurementKey;
    }
    return $this->getPropertyId();
  }

  /**
   * {@inheritdoc}
   */
  public function logInput(array $input) {
    $this->inputLog = $input;
  }

  /**
   * {@inheritdoc}
   */
  public function getLoggedInput() {
    return $this->inputLog;
  }

  /**
   * {@inheritdoc}
   */
  public function getUnitProperty() {
    return $this->unitProperty;
  }

  /**
   * {@inheritdoc}
   */
  public function getPropertyId() {
    return $this->unitProperty->getPluginId();
  }

  /**
   * {@inheritdoc}
   */
  public function getUnit() {
    return $this->unitDefinition;
  }

  /**
   * {@inheritdoc}
   */
  public function getUnitId() {
    return $this->unitDefinition['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultUnit() {
    return $this->getUnitProperty()->getDefaultUnit();
  }

  /**
   * {@inheritdoc}
   */
  public function getFactor() {
    return (float) $this->getUnit()['factor'];
  }

  /**
   * {@inheritdoc}
   */
  public function getUnitSymbol() {
    $symbol = $this->getUnit()['symbol'];
    if ((string) $symbol !== '') {
      return $symbol;
    }
    return $this->getUnitString();
  }

  /**
   * {@inheritdoc}
   */
  public function getUnitString() {
    if ((string) $this->getQuantity() === '1') {
      return $this->getUnit()['singular'];
    }
    return $this->getUnit()['plural'];
  }

  /**
   * {@inheritdoc}
   */
  public function setNumberFormat($precision = 6, $decimal = '.', $separator = ',') {
    $this->numberFormat = [
      'precision' => $precision,
      'decimal' => $decimal,
      'separator' => $separator,
    ];
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormatted($format = NULL) {
    if ($format === NULL) {
      $format = MeasurementInterface::FORMAT_QUANTITY;
    }

    $quantity = $this->getQuantity();

    if (count($this->numberFormat)) {
      $quantity = number_format(
        $quantity,
        $this->numberFormat['precision'],
        $this->numberFormat['decimal'],
        $this->numberFormat['separator']
      );
    }

    switch ($format) {
      case MeasurementInterface::FORMAT_QUANTITY:
        return $quantity;

      case MeasurementInterface::FORMAT_SYMBOL:
        return $quantity . ' ' . $this->getUnitSymbol();

      case MeasurementInterface::FORMAT_TEXT:
        return $quantity . ' ' . $this->getUnitString();

      case MeasurementInterface::FORMAT_SYMBOL_ONLY:
        if ($symbol = $this->getUnitSymbol()) {
          return $symbol;
        }
      case MeasurementInterface::FORMAT_TEXT_ONLY:
        return $this->getUnitString();

      default:
        throw Exception(sprintf('Measurement format "%s" not found.', $format));
    }
  }

}
