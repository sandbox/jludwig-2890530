<?php

namespace Drupal\unitsapi\Plugin;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\unitsapi\Exception\ConversionException;
use Drupal\unitsapi\Measurement;
use Drupal\unitsapi\MeasurementManager;
use Drupal\unitsapi\UnitsApiUnitManager;

/**
 * Base class for Units API derived property plugins.
 */
abstract class UnitsApiDerivedPropertyBase extends UnitsApiPropertyBase implements UnitsApiDerivedPropertyInterface {

  /**
   * Lists measurements required to calc derived qty that remain.
   *
   * When a measurement from this list is added to baseMeasurements via
   * $this->addBaseMeasurement(), it is removed from this list.
   *
   * This is a multidimensional array with the the top level key being the "key"
   * expected in base measurements being added to the property. Generally, it
   * will simply be the unit property plugin id that is expected.
   *
   * The second level array keys are as follows:
   * - unit: The plugin id of the expected unit.
   * - count: The number of this unit type expected. If omitted, a single unit
   *   of this type will be expected.
   * - property: The plugin id of the expected unit property. If omitted, the
   *   top level "key" will be the expected property.
   *
   * Note: The top level key must be a valid unit property plugin id if the
   * 'property' key is omitted in the second level.
   *
   * @var array
   */
  protected $requiredMeasurements = [];

  /**
   * Lists required measurements that have been added to this property.
   *
   * Measurements must be fully hydrated with qty and must be in the property's
   * baseMeasurements definition. When added a measurement is added (via
   * $this->addBaseMeasurements()), the count in the associated data in
   * $this->requiredMeasurements is decremented until the count is 0, when it is
   * removed from that list all together.
   *
   * @var \Drupal\unitsapi\Measurement[]
   */
  protected $baseMeasurements = [];

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, UnitsApiPropertyManager $units_manager, UnitsApiUnitManager $unit_manager, MeasurementManager $measurement_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $units_manager, $unit_manager, $measurement_manager);
    $this->createRequiredMeasurements();
  }

  /**
   * Sets up $this->requiredMeasurements based on baseMeasurements definition.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   Thrown if baseMeasurements definition  is invalid in some way.
   */
  protected function createRequiredMeasurements() {
    // Move to plugin validation.
    if (!isset($this->pluginDefinition['baseMeasurements'])) {
      throw new PluginException(sprintf('Plugin (%s) must contain "baseMeasurements" definition.', $this->pluginId));
    };
    if (!is_array($this->pluginDefinition['baseMeasurements'])) {
      throw new PluginException(sprintf('Plugin (%s) definition "baseMeasurements" must be an array.', $this->pluginId));
    }

    // The property manager validation should ensure that baseMeasurements is
    // an array and that the data there is set propertly etc.
    foreach ($this->pluginDefinition['baseMeasurements'] as $key => $data) {
      if (!isset($data['unit'])) {
        throw new PluginException(sprintf('Plugin (%s) "baseMeasurements" definition must contain units.', $this->pluginId));
      }
      $count = isset($data['count']) ? (int) $data['count'] : 1;
      $property = isset($data['property']) ? $data['property'] : $key;

      $this->requiredMeasurements[$key] = [
        'property' => $property,
        'unit' => $data['unit'],
        'count' => $count,
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getMeasurementKey(Measurement $measurement) {
    $key = $measurement->getPropertyId();
    if ($measurement->getMeasurementKey()) {
      $key = $measurement->getMeasurementKey();
    }
    return $key;
  }

  /**
   * {@inheritdoc}
   */
  public function hydrateDerivedMeasurement(Measurement $measurement) {
    if ($measurement->getPropertyId() != $this->pluginId) {
      throw new ConversionException('Cannot hydrate derived measurement. Measurement property type does not match property plugin property type.');
    }
    if (count($this->requiredMeasurements)) {
      throw new ConversionException('Cannot hydrate derived measurement. Required measurements remain.');
    }

    $derived_quantity = $this->calcDerivedQuantity();
    $measurement->setQuantity($derived_quantity);

    if ($measurement->getUnitId() !== $this->getDefaultUnit()) {
      /** @var Drupal\unitsapi\Measurement $from_measurement */
      $from_measurement = $this->measurementManager->createMeasurement($this->getDefaultUnit());
      $from_measurement->setQuantity($derived_quantity);

      /** @var Drupal\unitsapi\Measurement $to_measurement */
      $to_measurement = $this->convertUnit($from_measurement, $measurement->getUnitId());

      $measurement->setQuantity($to_measurement->getQuantity());
    }

  }

  /**
   * {@inheritdoc}
   */
  public function getCalculatedBase(Measurement $measurement, $to_unit = NULL) {
    if ($measurement->getQuantity() === NULL) {
      throw new ConversionException('Cannot calculate base measurement. No quantity found in the derived measurement.');
    }

    if (count($this->requiredMeasurements) !== 1) {
      throw new ConversionException('Cannot calculate base measurement. Base measurement calculation requires that exactly one required measurement remains.');
    }

    reset($this->requiredMeasurements);
    $base_quantity = $this->calcBaseMeasurement($measurement->getQuantity());
    $base_unit = current($this->requiredMeasurements)['unit'];

    /** @var Drupal\unitsapi\Measurement $base_measurement */
    $base_measurement = $this->measurementManager->createMeasurement($base_unit, $base_quantity);

    if ($to_unit === NULL) {
      return $base_measurement;
    }

    /** @var Drupal\unitsapi\Measurement $converted_base */
    return $this->convertUnit($base_measurement, $to_unit);
  }

  /**
   * {@inheritdoc}
   */
  public function addBaseMeasurement(Measurement $measurement) {
    $key = $measurement->getPropertyId();

    $this->validateBaseMeasurement($measurement);

    /** @var Drupal\unitsapi\Measurement $measurement_to_add */
    $measurement_to_add = clone $measurement;
    if ($measurement->getUnit() != $this->requiredMeasurements[$key]['unit']) {
      $measurement_to_add = $this->convertUnit($measurement, $this->requiredMeasurements[$key]['unit']);
      $measurement_to_add->setMeasurementKey($measurement->getMeasurementKey());
    }

    $this->baseMeasurements[$measurement->getMeasurementKey()][] = $measurement_to_add;
    $this->rmMeasurementFromRequirements($measurement);
  }

  /**
   * Validates that a base measurement can be added.
   *
   * @param \Drupal\unitsapi\Measurement $measurement
   *   The base measurement to be validated.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   Thrown if the passed in Measurement does not match a base measurement.
   */
  protected function validateBaseMeasurement(Measurement $measurement) {
    $key = $this->getMeasurementKey($measurement);
    if (!isset($this->requiredMeasurements[$key])) {
      throw new PluginException(sprintf('Plugin (%s) does not require a "%s" base measurement.', $this->pluginId, $key));
    }
    if ($this->requiredMeasurements[$key]['property'] != $measurement->getPropertyId()) {
      throw new PluginException(sprintf(
        'Plugin (%s) key "%s" base measurement is not a "%s" property type.',
        $this->pluginId,
        $key,
        $measurement->getPropertyId()
      ));
    }
  }

  /**
   * Decrements or removes a given Measuremetn from the requirements list.
   *
   * @param \Drupal\unitsapi\Measurement $measurement
   *   The measurement to remove or decrement.
   */
  protected function rmMeasurementFromRequirements(Measurement $measurement) {
    $key = $this->getMeasurementKey($measurement);
    $this->requiredMeasurements[$key]['count']--;
    if ($this->requiredMeasurements[$key]['count'] === 0) {
      unset($this->requiredMeasurements[$key]);
    }
  }

  /**
   * Returns the quantity of a matching base measurement.
   *
   * @param string $key
   *   The base measurement key.
   * @param int $delta
   *   The base measurement delta.
   *
   * @return float
   *   The quantity of the matching base measurement.
   */
  protected function getBaseQuantity($key, $delta = 0) {
    return (float) $this->baseMeasurements[$key][$delta]->getQuantity();
  }

  /**
   * Returns the quantity of this property based on the base measurements.
   *
   * @return float
   *   The quantity of this derived property.
   */
  abstract protected function calcDerivedQuantity();

  /**
   * Returns value of missing base measurement when given the derived quantity.
   *
   * @param int|float $quantity
   *   The quantity of this derived property.
   *
   * @return float
   *   The calculated value of the missing base measurement.
   */
  abstract protected function calcBaseMeasurement($quantity);

}
