<?php

namespace Drupal\unitsapi\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\unitsapi\Exception\ConversionException;
use Drupal\unitsapi\Measurement;
use Drupal\unitsapi\MeasurementManager;
use Drupal\unitsapi\UnitsApiUnitManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Units API property plugins.
 */
abstract class UnitsApiPropertyBase extends PluginBase implements UnitsApiPropertyInterface, ContainerFactoryPluginInterface {

  /**
   * The UnitsAPI property manager.
   *
   * @var \Drupal\unitsapi\UnitsApiPropertyManager
   */
  protected $unitsPropertyManager;

  /**
   * The UnitsAPI unit manager.
   *
   * @var \Drupal\unitsapi\UnitsApiUnitManager
   */
  protected $unitsManager;

  /**
   * The UnitsAPI measurement manager.
   *
   * @var \Drupal\unitsapi\MeasurementManager
   */
  protected $measurementManager;

  /**
   * Constructs a new UnitsApiProperty.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\unitsapi\UnitsApiPropertyManager $property_manager
   *   The UnitsAPI property manager.
   * @param \Drupal\unitsapi\UnitsApiUnitManager $unit_manager
   *   The UnitsAPI unit manager.
   * @param \Drupal\unitsapi\MeasurementManager $measurement_manager
   *   The UnitsAPI measurement manager.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, UnitsApiPropertyManager $property_manager, UnitsApiUnitManager $unit_manager, MeasurementManager $measurement_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->unitsPropertyManager = $property_manager;
    $this->unitsManager = $unit_manager;
    $this->measurementManager = $measurement_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.unitsapi_property'),
      $container->get('plugin.manager.unitsapi_unit'),
      $container->get('unitsapi.measurement_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function convertUnit(Measurement $from_measurement, $to_unit) {
    /** @var Drupal\unitsapi\Measurement $to_measurement */
    $to_measurement = $this->measurementManager->createMeasurement($to_unit);

    $this->validateUnitConversion($from_measurement, $to_measurement);

    $from_convert = $from_measurement->getQuantity() * $from_measurement->getFactor();
    $to_quantity = $from_convert / $to_measurement->getFactor();

    $to_measurement->setQuantity($to_quantity);
    return $to_measurement;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultUnit() {
    return $this->pluginDefinition['defaultUnit'];
  }

  /**
   * Validates that basic unit conversion doesn't have any issues.
   *
   * @param \Drupal\unitsapi\Measurement $from_measurement
   *   The measurement object containing the data to convert from.
   * @param \Drupal\unitsapi\Measurement $to_measurement
   *   The measurement object that the quantity conversion will apply to.
   *
   * @throws Drupal\unitsapi\Exception\ConversionException
   *   Thrown if a problem with the conversion is encountered.
   */
  protected function validateUnitConversion(Measurement $from_measurement, Measurement $to_measurement) {
    if ($from_measurement->getPropertyId() != $to_measurement->getPropertyId()) {
      throw new ConversionException(sprintf(
        'Units API cannot convert a "%s" to a "%s" because they are two different types of units.',
        $from_measurement->getPropertyId(),
        $to_measurement->getPropertyId()
      ));
    }
    if (!is_numeric($from_measurement->getQuantity())) {
      throw new ConversionException(sprintf(
        'Units API cannot convert because the from measurement quantity "%s" is not numeric.',
        $from_measurement->getQuantity()
      ));
    }
  }

}
